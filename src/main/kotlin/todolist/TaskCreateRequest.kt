package todolist

import com.fasterxml.jackson.annotation.JsonProperty

data class TaskCreateRequest(
        // アノテーションでJSONとの対応関係を指定
        @JsonProperty("content", required = true) val content: String
)