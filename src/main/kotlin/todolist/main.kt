package todolist

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import spark.Spark.*

fun main(args: Array<String>) {
    val objectMapper = ObjectMapper().registerKotlinModule()
    val jsonTransformer = JsonTransformer(objectMapper)
    val taskRepository = TaskRepository()
    val taskController = TaskController(objectMapper, taskRepository)

    path("tasks") {
        get("", taskController.index(), jsonTransformer)
        post("", taskController.create(), jsonTransformer)
        get("/:id", taskController.show(), jsonTransformer)
        patch("/:id", taskController.update(), jsonTransformer)
        delete("/:id", taskController.destory(), jsonTransformer)
    }
    get("/") { req, res ->
"""
<html>
    <head>
        <meta http-equiv="Access-Control-Allow-Origin" content="*">
        <meta http-equiv="Access-Control-Allow-Methods" content="*">
        <meta http-equiv="Access-Control-Allow-Headers" content="*">

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>
            var baseUrl = "./tasks";
            var outputJson = function(data) { ${'$'}("#result").val(JSON.stringify(JSON.parse(data), null, "    ")); }
            function createTask() {
                var content = ${'$'}("input[name='content']").val();
                console.log(content);
                if(content) {
                    ${'$'}.post(baseUrl, '{"content":"'+ content +'"}')
                        .done(outputJson);
                }
            }
            function updateTask() {
                var id = ${'$'}("input[name='id']").val();
                var content = ${'$'}("input[name='content']").val();
                var done = ${'$'}("input[name='done']").val();
                if(id) {
                    ${'$'}.ajax( {
                        url: baseUrl + "/" + id,
                        data: '{"content":"' + content + '", "done":"' + done + '"}',
                        type: "PATCH"
                    }).done(getTask);
                }
            }
            function deleteTask() {
                var id = ${'$'}("input[name='id']").val();
                if(id) {
                    ${'$'}.ajax( {
                        url: baseUrl + "/" + id,
                        type: "DELETE"
                    }).done(getlistTask);
                }
            }
            function getTask() {
                 var id = ${'$'}("input[name='id']").val();
                ${'$'}.get(baseUrl + "/" + id).done(outputJson)
            }
            function getlistTask() {
                ${'$'}.get(baseUrl).done(outputJson)
            }
        </script>
    </head>
    <body>
        <div style="margin-bottom: 5px">
            <table>
                <tr>
                    <td>ID</td><td><input type="text" name="id"/></td>
                </tr>
                <tr>
                    <td>CONTENT</td><td><input type="text" name="content"/></td>
                </tr>
                <tr>
                    <td>DONE</td><td><input type="text" name="done"/></td>
                </tr>
            </table>
        </div>
        <div>
            <span><a href="javascript:void(0);" onclick="createTask()">追加</a></span>
            <span><a href="javascript:void(0);" onclick="updateTask()">更新</a></span>
            <span><a href="javascript:void(0);" onclick="deleteTask()">削除</a></span>
            <span><a href="javascript:void(0);" onclick="getlistTask()">一覧</a></span>
            </table>
        </div>
        <div>
            <h3>結果出力</h3>
            <textarea id="result" style="border: 1px;" cols="200" rows="200"></textarea>
        </div>
    </body>
</html>
"""
    }
}
