package todolist

/**
 * タスクを表現するクラス
 * ・ID
 * ・タスクの内容
 * ・作業済みであるかどうかのフラグ
 */
data class Task(val id: Long,
                val content: String,
                val done: Boolean)